<?php
    include_once "../vendor/autoload.php";
    use App\Profile\profile;
    $obj = new profile();
	
	$usernameid = $obj->setData($_GET)->usernameid();
	$_GET['id'] = $usernameid;

$about_value=$obj->setData($_GET['id'])->aboutshow();

$hobbies_value=$obj->setData($_GET['id'])->hobbiesshow();

$fact_value=$obj->setData($_GET['id'])->factshow();

$edu_value= $obj->setData($_GET['id'])->educationshow();

$award_value= $obj->setData($_GET['id'])->awardshow();

$skill_value= $obj->setData($_GET['id'])->skillshow();

$setting_value= $obj->setData($_GET['id'])->settingshow();

$user_value= $obj->setData($_GET['id'])->usersshow();

$portfolio_value= $obj->setData($_GET['id'])->portfolioshow();

$service_value= $obj->setData($_GET['id'])->serviceshow();

$blog_value= $obj->setData($_GET['id'])->blogshow();

?>
<?php include_once "header.php";?>

<body>
<!-- Page preloader -->
<div id="page-loader">
    <canvas id="demo-canvas"></canvas>
</div>
<!-- container -->
<div id="hs-container" class="hs-container">

    <!-- Sidebar-->
    <div class="aside1">
        <a class="contact-button"><i class="fa fa-paper-plane"></i></a>
        <a href="../pdf.php?username=<?php echo $user_value['username'];?>" class="download-button"><i class="fa fa-cloud-download"></i></a>
        <div class="aside-content"><span class="part1"><?php echo $setting_value['fullname'];?></span><span class="part2">Academic Personal Vcard</span>
        </div>
    </div>
    <aside class="hs-menu" id="hs-menu">
        <!-- <canvas id="demo-canvas"></canvas> -->

        <!-- Profil Image-->
        <div class="hs-headline">
            <a id="my-link" href="#my-panel"><i class="fa fa-bars"></i></a>
            <a href="#" class="download"><i class="fa fa-cloud-download"></i></a>
            <div class="img-wrap">
                <img src="../../assets/upload_image/<?php echo $setting_value['img'];?>" alt="" width="150" height="150" />
            </div>
            <div class="profile_info">
                <h1><?php echo $setting_value['fullname'];?></h1>
                <h4><?php echo $setting_value['title'];?></h4>
                <h6><span class="fa fa-location-arrow"></span>&nbsp;&nbsp;&nbsp;<?php echo $setting_value['address'];?></h6>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="separator-aside"></div>
        <!-- End Profil Image-->

        <!-- menu -->
        <nav>
            <a href="#section1"><span class="menu_name">ABOUT</span><span class="fa fa-home"></span> </a>
            <a href="#section2"><span class="menu_name">RESUME</span><span class="fa fa-newspaper-o"></span> </a>
            <a href="#section3"><span class="menu_name">BLOG POSTS</span><span class="fa fa-pencil"></span> </a>
            <a href="#section4"><span class="menu_name">SERVICES</span><span class="fa fa-flask"></span> </a>
            <a href="#section6"><span class="menu_name">SKILLS</span><span class="fa fa-diamond"></span> </a>
            <a href="#section7"><span class="menu_name">PORTFOLIOS</span><span class="fa fa-archive"></span> </a>
            <a href="#section8"><span class="menu_name">CONTACT</span><span class="fa fa-paper-plane"></span> </a>
        </nav>
        <!-- end menu-->
    </aside>
    <!-- End sidebar -->

    <!-- Go To Top Button -->
    <a href="#hs-menu" class="hs-totop-link"><i class="fa fa-chevron-up"></i></a>
    <!-- End Go To Top Button -->

    <!-- hs-content-scroller -->
    <div class="hs-content-scroller">
        <!-- Header -->
        <div id="header_container">
            <div id="header">
                <div><a class="home"><i class="fa fa-home"></i></a>
                </div>
                <div><a href="" class="previous-page arrow"><i class="fa fa-angle-left"></i></a>
                </div>
                <div><a href="" class="next-page arrow"><i class="fa fa-angle-right"></i></a>
                </div>
                <!-- News scroll -->
                <div class="news-scroll">
                    <span><i class="fa fa-line-chart"></i>SUMMARY : </span>
                    <ul id="marquee" class="marquee">
                        <li><?php echo $setting_value['description'];?><?php echo $about_value['bio'];?></li>
                        <li><?php echo $setting_value['description'];?><?php echo $about_value['bio'];?></li>
                        <li><?php echo $setting_value['description'];?><?php echo $about_value['bio'];?></li>
                    </ul>
                </div>
                <!-- End News scroll -->
            </div>
        </div>
        <!-- End Header -->

        <!-- hs-content-wrapper -->
        <div class="hs-content-wrapper">
            <!-- About section -->
            <article class="hs-content about-section" id="section1">
                <span class="sec-icon fa fa-home"></span>
                <div class="hs-inner">
                    <span class="before-title">.01</span>
                    <h2>ABOUT</h2>
                    <span class="content-title">PERSONAL DETAILS</span>
                    <div class="aboutInfo-contanier">
                        <div class="about-card">
                            <div class="face2 card-face">
                                <div id="cd-google-map">
                                    <div id="google-container"></div>
                                    <div id="cd-zoom-in"></div>
                                    <div id="cd-zoom-out"></div>
                                    <address>8690 Paul Street, San fransico</address>
                                    <div class="back-cover" data-card-back="data-card-back"><i class="fa fa-long-arrow-left"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="face1 card-face">
                                <div class="about-cover card-face">
                                    <a class="map-location" data-card-front="data-card-front"><img src="../images/map-icon.png" alt="">
                                    </a>
                                    <div class="about-details">
                                        <div><span class="fa fa-inbox"></span><span class="detail"><?php echo $user_value['email'];?></span>
                                        </div>
                                        <div><span class="fa fa-phone"></span><span class="detail"><?php echo $about_value['phone'];?></span>
                                        </div>
                                    </div>

                                    <div class="cover-content-wrapper">
                                            <span class="about-description">Hello. I am a<span class="rw-words">
                                                <span><strong><?php echo $about_value['title'];?></strong></span>
                                                <span><strong><?php echo $about_value['title'];?></strong></span>
                                                <span><strong><?php echo $about_value['title'];?></strong></span>
                                                <span><strong><?php echo $about_value['title'];?></strong></span><br/>
                                            </span><?php echo $setting_value['description'];?></span>
                                        <span class="status">
                                            <span class="fa fa-circle"></span>
                                            <span class="text">Available as <strong>freelance</strong></span>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="more-details">
                            <div class="tabbable tabs-vertical tabs-left">
                                <ul id="myTab" class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#bio" data-toggle="tab">Bio</a>
                                    </li>
                                    <li>
                                        <a href="#hobbies" data-toggle="tab">Hobbies</a>
                                    </li>
                                    <li>
                                        <a href="#facts" data-toggle="tab">Facts</a>
                                    </li>
                                </ul>
                                <div id="myTabContent" class="tab-content">

                                    <div class="tab-pane fade in active" id="bio">
                                        <h3>BIO</h3>
                                        <h4>ABOUT ME</h4>
                                        <p><?php echo $about_value['bio'];?></p>
                                    </div>
                                    <div class="tab-pane fade" id="hobbies">
                                        <h3>HOBBIES</h3>
                                        <?php foreach ($hobbies_value as $hobby){?>
                                            <div class="hobbie-wrapper row">
                                                <div class="hobbie-icon col-md-4"><img style="width: 80px; height: 80px; border-radius: 20px;"
                                                                                       src="../../assets/upload_image/<?php echo $hobby['img'];?>"/>
                                                </div>
                                                <div class="hobbie-description col-md-8">
                                                    <h3><?php echo $hobby['title'];?></h3>
                                                    <p><?php echo $hobby['description'];?></p>
                                                </div>
                                                <div style="clear:both;"></div>

                                            </div>
                                        <?php } ?>
                                    </div>

                                    <div class="tab-pane fade" id="facts">
                                        <h3>FACTS</h3>
                                        <h4>NUMBERS ABOUT ME</h4>
                                        <div class="facts-wrapper col-md-6">
                                            <?php foreach ($fact_value as $fact){?>
                                                <div class="facts-icon">
                                                    <img style="width: 80px; height: 80px; border-radius: 20px;" src="../../assets/upload_image/<?php echo $fact['img'];?>"/>
                                                </div>
                                                <div class="facts-number"><?php echo $fact['no_of_items'];?></div>
                                                <div class="facts-description"><?php echo $fact['title'];?></div>
                                            <?php } ?>
                                        </div>
                                        <div style="clear:both;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </article>
            <!-- End About Section -->

            <!-- Resume Section -->
            <article class="hs-content resume-section" id="section2">
                <span class="sec-icon fa fa-newspaper-o"></span>
                <div class="hs-inner">
                    <span class="before-title">.02</span>
                    <h2>RESUME</h2>
                    <!-- Resume Wrapper -->
                    <div class="resume-wrapper">
                        <ul class="resume">
                            <!-- Resume timeline -->
                            <li class="time-label">
                                <span class="content-title">EDUCATION</span>
                            </li>
                            <?php foreach ($edu_value as $edu){?>
                                <li>
                                    <div class="resume-tag">
                                        <span class="fa fa-graduation-cap"></span>
                                        <div class="resume-date">
                                            <span><?php echo $edu['passing_year'];?></span>
                                        </div>
                                    </div>
                                    <div class="timeline-item">
                                        <span class="timeline-location"><i class="fa fa-globe" aria-hidden="true"></i><span><?php echo $edu['education_board'];?></span></span>
                                        <h3 class="timeline-header"><?php echo $edu['title'];?></h3>
                                        <div class="timeline-body">
                                            <h4><?php echo $edu['institute'];?></h4>
                                            <span><p style="display: inline; font-size: 13px;font-weight: 700; margin-right: 10px">Result:</p><?php echo $edu['result'];?></span><br/>
                                            <span><p style="display: inline; font-size: 13px;font-weight: 700; margin-right: 10px">Subject:</p><?php echo $edu['main_subject'];?></span><br/>
                                            <span><p style="display: inline; font-size: 13px;font-weight: 700; margin-right: 10px">Course Duration:</p><?php echo $edu['course_duration'];?></span>
                                        </div>

                                    </div>

                                </li>
                            <?php } ?>
                            <li class="time-label">
                                <span class="content-title">HONORS AND AWARDS</span>
                            </li>
                            <?php foreach ($award_value as $award){ ?>
                                <li>
                                    <div class="resume-tag">
                                        <span class="fa fa-star-o"></span>
                                        <div class="resume-date">
                                            <span><?php echo $award['year'];?></span>
                                        </div>
                                    </div>
                                    <div class="timeline-item">
                                        <span class="timeline-location"><i class="fa fa-map-marker"></i><?php echo $award['location'];?></span>
                                        <h3 class="timeline-header"><?php echo $award['organization'];?></h3>
                                        <div class="timeline-body">
                                            <h4><?php echo $award['title'];?></h4>
                                            <span><?php echo $award['description'];?>.</span>
                                        </div>
                                    </div>
                                </li>
                            <?php } ?>
                            <!-- End Resume timeline -->
                        </ul>
                    </div>
                    <!-- End Resume Wrapper -->
                </div>
            </article>
            <!-- End Resume Section-->

            <!-- Blog Post Section -->
            <article class="hs-content publication-section" id="section3">
                <span class="sec-icon fa fa-pencil"></span>
                <div class="hs-inner">
                    <span class="before-title">.03</span>
                    <h2>BLOG POSTS</h2>
                    <!-- Filter/Sort Menu -->
                    <span class="content-title">BLOG LIST</span>
                    <!-- End Filter/Sort Menu -->

                    <!-- publication wrapper -->
                    <div id="mygrid">
                        <!-- publication item -->
                        <?php foreach($blog_value as $blog){?>
                            <div class="publication_item" data-groups='["all","Social"]' data-date-publication="2007-12-02">
                                <div class="media">
                                    <a href=".publication-detail2" class="ex-link open_popup" data-effect="mfp-zoom-out"><i class="fa fa-plus-square-o"></i></a>
                                    <div class="date pull-left">
                                        <h3 style="font-size: 13px;color: white;padding: 8px 10px"><?php echo $blog['tags']; ?></h3>
                                    </div>
                                    <div class="media-body">
                                        <h3><?php echo $blog['title'];?></h3>
                                        <span class="publication_description"><?php echo $blog['description'];?></span> </div>
                                    <hr style="margin:8px auto">
                                    <span class="label label-success" style="font-size: 10px"><?php echo $blog['categories'];?></span>
                                    <span class="publication_authors"><strong><?php echo $setting_value['fullname'];?></strong><?php echo $setting_value['title'];?></span>
                                </div>
                                <div class="mfp-hide mfp-with-anim publication-detail2 publication-detail">
<!--                                    <div class="image_work">-->
<!--                                        <img style="width: 80px; height: 80px; border-radius: 20px;" src="../../assets/upload_image/--><?php //echo $blog['img'];?><!--" alt="img" width="480" height="200"/>-->
<!--                                    </div>-->
                                    <div class="project_content">
                                        <h3 class="publication_title"><?php echo $blog['title'];?></h3>
                                        <span class="publication_authors"><strong><?php echo $setting_value['fullname'];?></strong><?php echo $setting_value['title'];?></span>
                                        <span class="label label-success"><?php echo $blog['categories'];?></span>
                                        <p><?php echo $blog['description'];?></p>
                                    </div>
                                    <a class="ext_link" href="#"><i class="fa fa-external-link"></i></a>
                                    <div style="clear:both"></div>
                                </div>
                            </div>
                        <?php } ?>
                        <!-- End Publication item -->
                    </div>
                    <!-- End Publication Wrapper -->
                </div>
                <div class="clear"></div>
            </article>
            <!-- End Publication Section -->

            <!-- SERVICES Section -->
            <article class="hs-content research-section" id="section4">
                <span class="sec-icon fa fa-flask"></span>
                <div class="hs-inner">
                    <span class="before-title">.04</span>
                    <h2>SERVICES</h2>
                    <span class="content-title">SERVICE TEAM</span>
                    <div class="team_wrapper">
                        <?php foreach((array) $service_value as $service){ ?>
                            <div class="team-card-container">
                                <div class="card">
                                    <div class="front"><img width="220px" height="250px" src="../../assets/upload_image/<?php echo $service['img'];?>"/>
                                        <div class="front-detail">
                                            <h3 style="background-color: #1A237E;padding:5px 8px;opacity: 0.8;margin-bottom: 3px;color: white"><?php echo $service['title'];?></h3>
                                        </div>
                                    </div>
                                    <div class="back">
                                        <p><?php echo $service['description'];?></p>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </article>
            <!-- End Research Section -->
            <!-- Skills Section -->
            <article class="hs-content skills-section" id="section6">
                <span class="sec-icon fa fa-diamond"></span>
                <div class="hs-inner">
                    <span class="before-title">.06</span>
                    <h2>SKILLS</h2>
                    <?php foreach($skill_value as $skill){ ?>

                        <span class="content-title"><?php echo $skill['title'];?></span>
                        <div class="skolls">
                            <span class="skill-description"><?php echo $skill['description'];?></span>
                            <div class="bar-main-container">
                                <div class="wrap">
                                    <div class="bar-percentage"><?php echo $skill['level'];?>%</div>
                                    <span class="skill-detail"><i class="fa fa-bar-chart"></i>EXPERIENCE AREA : <?php echo $skill['experience_area'];?></span><span class="skill-detail"><i class="fa fa-binoculars"></i>EXPERIENCE :<?php echo $skill['experience'];?></span>
                                    <div class="bar-container">
                                        <div class="bar"></div>
                                    </div>
                                    <div style="clear:both;"></div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </article>
            <!-- End Skills Section -->

            <!-- Works Section -->
            <article class="hs-content works-section" id="section7">
                <span class="sec-icon fa fa-archive"></span>
                <div class="hs-inner">
                    <span class="before-title">.07</span>
                    <h2>PORTFOLIOS</h2>
                    <div class="portfolio">
                        <!-- Portfolio Item -->
                        <?php foreach($portfolio_value as $portfolio){ ?>
                            <figure class="effect-milo">
                                <img width="282px" height="222px" src="../../assets/upload_image/<?php echo $portfolio['img'];?>" alt="img11"/>
                                <figcaption>
                                    <span><h3 style="background-color: #1A237E;padding:5px 8px;opacity: 0.8;margin-bottom: 3px;color: white"><?php echo $portfolio['category'];?></h3></span>
                                    <div class="portfolio_button">
                                        <a href=".work1" class="open_popup" data-effect="mfp-zoom-out">
                                            <i class="hovicon effect-9 sub-b"><i class="fa fa-search"></i></i>
                                        </a>
                                    </div>
                                    <div class="mfp-hide mfp-with-anim work_desc work1">
                                        <div class="col-md-6">
                                            <div class="image_work">
                                                <img src="../../assets/upload_image/<?php echo $portfolio['img'];?>" alt="img" width="560" height="420">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="project_content">
                                                <h2 class="project_title"><?php echo $portfolio['title'];?></h2>
                                                <p class="project_desc"><?php echo $portfolio['description'];?></p>
                                            </div>
                                        </div>
                                        <a class="ext_link" href="#"><i class="fa fa-external-link"></i></a>
                                        <div style="clear:both"></div>
                                    </div>
                                </figcaption>
                            </figure>
                        <?php }?>
                        <!-- End Portfolio Item -->
                    </div>
                    <!-- End Portfolio Wrapper -->
                </div>
            </article>
            <!-- End Works Section -->

            <!-- Contact Section -->
            <article class="hs-content contact-section" id="section8">
                <span class="sec-icon fa fa-paper-plane"></span>
                <div class="hs-inner">
                    <span class="before-title">.08</span>
                    <h2>CONTACT</h2>
                    <div class="contact_info">
                        <h3>Get in touch</h3>
                        <hr>
                        <h5>Use the form below to get in touch</h5>

                        <hr>
                    </div>
                    <!-- Contact Form -->
                    <fieldset id="contact_form">
                        <div id="result"></div>
                        <input type="text" name="name" id="name" placeholder="NAME" />
                        <input type="email" name="email" id="email" placeholder="EMAIL" />
                        <textarea name="message" id="message" placeholder="MESSAGE"></textarea>
                        <span class="submit_btn" id="submit_btn">SEND MESSAGE</span>
                    </fieldset>
                    <!-- End Contact Form -->
                </div>
            </article>
            <!-- End Contact Section -->
        </div>
        <!-- End hs-content-wrapper -->
    </div>
    <!-- End hs-content-scroller -->
</div>
<!-- End container -->
<div id="my-panel">
</div>

<!-- PLUGIN SCRIPTS -->
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/default.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="../js/watch.js"></script>
    <script type="text/javascript" src="../js/layout.js"></script>
    <script type="text/javascript" src="../js/main.js"></script>

    <!-- END PLUGIN SCRIPTS -->
</body>

</html>
