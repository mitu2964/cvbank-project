<?php
include_once ("vendor/autoload.php");
use App\Profile\profile;
$obj = new profile();
$perpage = 2;
if (!empty($_GET['page'])){
    $pagenumber = $_GET['page'];
}else{
    $pagenumber = 0;
}
if (!empty($_GET['page'])){
    $offset = $perpage * $pagenumber;
}else{
    $offset = 0;
}

$value = $obj->setData($_GET)->searchresult($perpage,$offset);
$totalrow = count($value);
if($totalrow <= 1){
	$_SESSION['sr'] = "No Match Data Found";
	header('location:index.php');
}
$numberofpage = ceil($totalrow/$perpage);
array_pop($value);
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Searching Result</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="assets/js/core/app.js"></script>
	<!-- /theme JS files -->

</head>

<body>
			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

<!-- Search field -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Website search results</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<form action="search_result.php" method="get" class="main-search">
								<div class="input-group content-group">
									<div class="has-feedback has-feedback-left">
										<input type="text" class="form-control input-xlg" name="keywords"required>
										<div class="form-control-feedback">
											<i class="icon-search4 text-muted text-size-base"></i>
										</div>
									</div>

									<div class="input-group-btn">
										<button type="submit" class="btn btn-primary btn-xlg">Search</button>
									</div>
								</div>

								<div class="row search-option-buttons">
									<div class="col-sm-6">
										<ul class="list-inline list-inline-condensed no-margin-bottom">
											<li class="dropdown">
												
												
											<div class="form-group">
										<label>Search By : </label>											
											<select class="select-border-color border-warning" name="searchvalue">
											<optgroup label="Search From Reflection">
												<option value="educations">Educations</option>
												<option value="skills">Skills</option>
												
											</optgroup>
                                            </select>
											</div>
													
												<ul class="dropdown-menu">
												<select></select>
												</ul>
											</li>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- /search field -->
					<!-- Search results -->
					<div class="panel panel-body">
						<p class="text-muted text-size-small">About 827,000 results (0.34 seconds)</p>

						<hr>
						<div class="row">
						<div class="col-lg-8">
							<div class="panel panel-flat">
							     <div class="panel-body">
								 <?php foreach ($value as $key => $item ){ ?>
									<div class="media">
										<div class="media-left">
											<a href="cv/profile/<?php echo $item['username']?>" target="_blank"><img src="assets/upload_image/<?php echo $item['simg']?>" class="img-circle" alt=""></a>
										</div>
										<div class="media-body">
											<h6 class="media-heading"><a href="cv/profile/<?php echo $item['username']?>" target="_blank"><?php echo $item['name']?> <span class="media-annotation dotted"><?php echo $item['ab_title']?></a></span></h6>
											<ul class="list-inline list-inline-separate text-muted">
				                    			<li><a href="cv/profile/<?php echo $item['username']?>" class="text-success" target="_blank">http://tubehsckbd.com/cv/profile/<?php echo $item['username']?></a></li>
				                    		</ul>
											<?php echo $item['ab_bio']?>
										</div>
								 <?php } ?>
									
									</div>
									
								</div>
								
							</div>
							<ul class="pagination pagination-flat pagination-xs no-margin-bottom">
									<li class="disabled"><a href="#">&larr;</a></li>
									<li class="disabled"><a href="#">Pagination : </a></li>
                                    <?php
									$value1=$_GET['keywords'];
									$data=$_GET['searchvalue'];
                                    for ($i = 0; $i < $numberofpage; $i++)
                                    {
                                        echo '<li><a href="search_result.php?page='.$i.'&keywords='.$value1.'&searchvalue='.$data.'">'.$i.'</a></li>';
                                    }
                                    ?>
								</ul>
						</div>

						</div></div></div>
	<!-- /page container -->

</body>
</html>
