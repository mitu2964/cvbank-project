<?php
include_once ("../../vendor/autoload.php");
use App\Portfolio\portfolio;
session_start();
 $id=$_POST['id'];

if ($_SERVER['REQUEST_METHOD'] == 'POST'){

    $imgFile = $_FILES['user_image']['name'];
    $tmp_dir = $_FILES['user_image']['tmp_name'];
    $imgSize = $_FILES['user_image']['size'];


    if(empty($_POST['title'])){
        $_SESSION['portfolio-message'] = "Please Enter Title.";
        header("location:add_portfolio.php?id=$id");
    }
    else if(empty($_POST['description'])){
        $_SESSION['portfolio-message'] = "Please Enter Your Description.";
        header("location:add_portfolio.php?id=$id");
    }
    else if(empty($imgFile)){
        $_SESSION['portfolio-message'] = "Please Select Image File.";
        header("location:add_portfolio.php?id=$id");
    }
    else if(empty($_POST['category'])){
        $_SESSION['portfolio-message'] = "Please Enter Your Description.";
        header("location:add_portfolio.php?id=$id");
    }
    else
    {
        $upload_dir = '../../assets/upload_image/';
        $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION));
        $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
        $userpic = rand(1000,1000000).'portfolios'.".".$imgExt;
        if(in_array($imgExt, $valid_extensions)){
            if($imgSize < 5000000)    {
                move_uploaded_file($tmp_dir,$upload_dir.$userpic);
                $_POST['img'] = $userpic;
                $reg = new portfolio();
                $reg->setData($_POST)->store();
            }
            else{
                $_SESSION['portfolio-message'] = "Sorry, your file is too large.";
                header("location:add_portfolio.php?id=$id");
            }
        }
        else{
            $_SESSION['portfolio-message'] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            header("location:add_portfolio.php?id=$id");
        }
    }
}else {
    header("location:add_portfolio.php?id=$id");
}