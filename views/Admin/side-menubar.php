
<body>
<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="../Admin/dashboard.php"><img src="../../assets/images/logo_light.png" alt=""></a>
        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">

            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="../../assets/images/logo.jpg" alt="">
							<span>
                            <?php
                               echo $_SESSION['user_info']['username'];
                            ?>
							</span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="../../cv/profile/<?php echo $_SESSION['user_info']['username'];?>" target="_blank"><i class="icon-user-plus"></i> My profile</a></li>
                    <li class="divider"></li>
                    <li><a href="../../logout.php"><i class="icon-switch2"></i> Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->
                <div class="sidebar-user">
                    <div class="category-content">
                        <div class="media">
                            <a href="#" class="media-left"><img src="../../assets/images/logo.jpg"
                                                                class="img-circle img-sm" alt=""></a>

                            <div class="media-body">
										<span class="media-heading text-semibold">
                                            <?php
                                            echo $_SESSION['user_info']['username'];
                                            ?>
										</span>

                                <div class="text-size-mini text-muted">
                                    <i class="icon-pin text-size-small"></i> &nbsp;Dhaka,Bangladesh
                                </div>
                            </div>

                            <div class="media-right media-middle"></div>
                        </div>
                    </div>
                </div>
                <!-- /user menu -->

<!-- Main navigation -->
<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">
            <?php if($_SESSION['user_info']['user_role']==1){ ?>
            <li class="active"><a href="../Admin/dashboard.php"><i class="icon-home4"></i>
                    <span>Dashboard</span></a>
            </li>
            <li>
                <a href="#"><i class="icon-spinner3 spinner"></i> <span>Settings</span></a>
                <ul>
                    <li><a href="../Setting/update_setting.php?id=<?php echo $_SESSION['user_info']['id']; ?>">Setting</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-stack2"></i> <span>About</span></a>
                <ul>
                    <li><a href="../About/update_about.php?id=<?php echo $_SESSION['user_info']['id']; ?>">About Me</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-stack"></i> <span>Hobbies</span></a>
                <ul>
                    <li><a href="../Hobbies/hobbies_view.php?id=<?php echo $_SESSION['user_info']['id']; ?>">My Hobbies</a></li>
                    <li><a href="../Hobbies/add_hobbies.php?id=<?php echo $_SESSION['user_info']['id']; ?>">Add New Hobby</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-spinner10 spinner"></i> <span>Facts</span></a>
                <ul>
                    <li><a href="../Facts/fact_view.php?id=<?php echo $_SESSION['user_info']['id']; ?>">All Fact</a></li>
                    <li><a href="../Facts/add_fact.php?id=<?php echo $_SESSION['user_info']['id']; ?>">Add New Fact</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-select2"></i> <span>Education</span></a>
                <ul>
                    <li><a href="../Education/education_view.php?id=<?php echo $_SESSION['user_info']['id']; ?>">My Education</a></li>
                    <li><a href="../Education/add_education.php?id=<?php echo $_SESSION['user_info']['id']; ?>">Add New Education</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-sphere"></i> <span>Experiences</span></a>
                <ul>
                    <li><a href="../Experience/experience_view.php?id=<?php echo $_SESSION['user_info']['id']; ?>">My Experiences</a></li>
                    <li><a href="../Experience/add_experience.php?id=<?php echo $_SESSION['user_info']['id']; ?>">Add New Experience</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-spinner3 spinner"></i> <span>Awards</span></a>
                <ul>
                    <li><a href="../Award/award_view.php?id=<?php echo $_SESSION['user_info']['id']; ?>">My Awards</a></li>
                    <li><a href="../Award/add_award.php?id=<?php echo $_SESSION['user_info']['id']; ?>">Add New Award</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-tree5"></i> <span>Blogs</span></a>
                <ul>
                    <li><a href="../Blogs/blog_view.php?id=<?php echo $_SESSION['user_info']['id']; ?>">My Blogs</a></li>
                    <li><a href="../Blogs/add_blog.php?id=<?php echo $_SESSION['user_info']['id']; ?>">Add New Blog</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-alignment-unalign"></i> <span>Servies</span></a>
                <ul>
                    <li><a href="../Services/service_view.php?id=<?php echo $_SESSION['user_info']['id']; ?>">All Service</a></li>
                    <li><a href="../Services/add_service.php?id=<?php echo $_SESSION['user_info']['id']; ?>">Add New Service</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-select2"></i> <span>Skills</span></a>
                <ul>
                    <li><a href="../Skills/skill_view.php?id=<?php echo $_SESSION['user_info']['id']; ?>">My Skills</a></li>
                    <li><a href="../Skills/add_skill.php?id=<?php echo $_SESSION['user_info']['id']; ?>">Add New Skill</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-user-plus"></i> <span>Portfolios</span></a>
                <ul>
                    <li><a href="../Portfolio/portfolio_view.php?id=<?php echo $_SESSION['user_info']['id']; ?>">My Portfolios</a></li>
                    <li><a href="../Portfolio/add_portfolio.php?id=<?php echo $_SESSION['user_info']['id']; ?>">Add New Portfolio</a></li>
                </ul>
            </li>
<?php }else{?>
                <li class="active"><a href="../Admin/dashboard.php"><i class="icon-home4"></i>
                        <span>Dashboard
                        </span></a>
                </li>
                <li>
                    <a href="#"><i class="icon-spinner3 spinner"></i> <span>All Admin</span></a>
                    <ul>
                        <li><a href="../adminpanel/adminlist.php">Admin List</a></li>
                        <li><a href="../adminpanel/add_admin.php">Add New Admin</a></li>
                    </ul>
                </li><li>
                    <a href="#"><i class="icon-spinner3 spinner"></i> <span>All User</span></a>
                    <ul>
                        <li><a href="../adminpanel/userlist.php">User List</a></li>
                    </ul>
                </li>
            <?php }?>
        </ul>
    </div>
</div>
</div>
</div>
<!-- /main sidebar -->
<!-- /main navigation -->