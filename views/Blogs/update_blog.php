<?php
include_once ("../../vendor/autoload.php");
use App\Blog\blog;
$obj = new blog();
$value=$obj->setData($_GET)->updateshow();
//echo "<pre>";
//print_r($value);
//die();
?>
<?php
if (!empty($_SESSION['user_info'])) {
?>
<?php include_once"../header.php"; ?>


<?php include_once("../Admin/side-menubar.php"); ?>



<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span
                        class="text-semibold">Home</span> - Dashboard</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <div class="row">
            <div class="col-md-10">

                <!-- Basic layout-->
                <form action="blog_update.php" method="post" class="form-horizontal">
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h2 class="panel-title">Update Your Data
                                <span class="label label-success position-right" style="font-size: 14px">
                                    <?php
                                        if (isset($_SESSION['update-message'])){
                                            echo $_SESSION['update-message'];
                                            unset($_SESSION['update-message']);
                                        }
                                    ?>
                                </span>
                            </h2>
                            <div class="heading-elements">
                                <span class="label label-primary heading-text"><a href="../Blogs/add_blog.php?id=<?php echo $_SESSION['user_info']['id'];?>" style="color: black;font-size: 14px" >Add New Blog</a></span>
                                <span class="label label-primary heading-text"><a href="../Blogs/blog_view.php?id=<?php echo $_SESSION['user_info']['id'];?>" style="color: black;font-size: 14px" >My Blogs</a></span>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Title:</label>
                                <div class="col-lg-8">
                                    <input type="text" name="title" value="<?php echo $value['title'] ;?>" class="form-control" placeholder="Write your title ">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label"> Description:</label>
                                <div class="col-lg-8">
                                    <textarea rows="5" cols="5" name="description" class="form-control" placeholder="Enter your description here"><?php echo $value['description']; ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label"> Tags:</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="tags" value="<?php echo $value['tags']; ?>" placeholder="Tags"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Categories:</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="categories" value="<?php echo $value['categories']; ?>" placeholder="Categories"/>
                                </div>
                            </div>


                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Update<i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $_SESSION['user_info']['id'];?>"/>
                    <input type="hidden" name="postid" value="<?php echo $value['id']; ?>">
                </form>
                <!-- /basic layout -->
            </div>
            <!-- /main charts -->

            <?php
            include_once("../footer.php");
            ?>
            <?php
            } else{
                $_SESSION['fail']= "You are not authorized!";
                header('location:../../../index.php');
            }
            ?>
