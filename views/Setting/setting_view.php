<?php
    include_once "../../vendor/autoload.php";
    use App\setting\setting;
    $obj= new setting();
    $value= $obj->setData($_GET)->show();
?>
<?php
    if(!empty($_SESSION['user_info'])){
?>
<?php include_once"../header.php"; ?>

<?php include_once("../Admin/side-menubar.php"); ?>

    <!-- Main content -->
    <div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span
                            class="text-semibold">Home</span> - Dashboard</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
    <div class="row">
    <div class="col-md-12">

        <!-- Basic layout-->

        <h2>Hobbies Table</h2>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Full Name</th>
                    <th>Description</th>
                    <th>Address</th>
                    <th>Image</th>
                    <th>EDIT/DELETE</th>
                </tr>
                </thead>
                <tbody>
                 <?php foreach ($value as $key => $item ){ ?>
                <tr>
                    <td><?php echo $item['title'];?></td>
                    <td><?php echo $item['fullname'];?></td>
                    <td><?php echo $item['description'];?></td>
                    <td><?php echo $item['address'];?></td>
                    <td><img style="width: 40px;height:40px" src="../../assets/upload_image/<?php echo $item['img']; ?>"
                             alt="<?php echo $item['img']; ?>" class="img-responsive"/></td>
                    <td>
                        <a href="../Setting/update_setting.php?id=<?php echo $item['id']; ?>" class="btn btn-success">Edit</a>
                        <a href="../Setting/delete_setting.php?id=<?php echo $item['id']; ?>" class="btn btn-danger">DELETE</a>
                    </td>
                </tr>
                 <?php } ?>
                </tbody>
            </table>
        </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="alert alert-success no-border">
                        <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                        <span class="text-semibold"><?php
                            if (isset($_SESSION['message'])){
                                echo $_SESSION['message'];
                                unset($_SESSION['message']);
                            } ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /main charts -->

<?php
include_once("../footer.php");
?>

    		<?php
	} else{
		$_SESSION['fail']= "You are not authorized!";
		header('location:../../../index.php');
	}

?>