<?php
include_once "../../vendor/autoload.php";
use App\adminpanel\userlist;
$obj = new userlist();
$value = $obj->username();
//echo "<pre>";
//    print_r($value);
////    die();
?>
<?php
if (!empty($_SESSION['user_info'])) { ?>

    <?php
//    echo "<pre>";
//print_r($_SESSION['user_info'])
    ?>


    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>DashBoard :: User LIST</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="../../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
        <link href="../../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
        <link href="../../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script type="text/javascript" src="../../assets/js/plugins/forms/selects/select2.min.js"></script>

        <script type="text/javascript" src="../../assets/js/core/app.js"></script>
        <script type="text/javascript" src="../../assets/js/pages/datatables_basic.js"></script>
        <!-- /theme JS files -->

    </head>

    <?php
    include "../Admin/side-menubar.php";
    ?>

    <!-- Main content -->
    <div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span
                        class="text-semibold">Home</span> - Dashboard</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="../Admin/dashboard.php"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h2 class="panel-title">User List
                    <span class="label label-success position-right" style="font-size: 14px"><?php
                        if (isset($_SESSION['message'])){
                            echo $_SESSION['message'];
                            unset($_SESSION['message']);
                        } ?>
                                    </span>
                </h2>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Bio</th>
                    <th>User</th>
                    <th>Status</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($value as $key => $item){?>
                <tr>
                    <td><?php echo $item['sname']?></td>
                    <td><?php echo $item['abio']?></td>
                    <td><?php echo $item['uname']?></td>
                    <td><span class="label label-success">Active</span></td>
                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="#"><i class="icon-file-pdf"></i> Export to .pdf</a></li>
                                    <li><a href="../../cv/profile/<?php echo $item['uname'];?>" target="_blank"><i class="icon-file-excel"></i> SHOW</a></li>
                                    <li><a href="userlist_delete.php?id=<?php echo $item['uid'];?>"><i class="icon-file-word"></i> DELETE</a></li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <!-- /basic datatable -->

    </div>
    <!-- /main charts -->

    <?php
    include_once("../footer.php");
    ?>
    <?php
} else{
    $_SESSION['fail']= "You are not authorized!";
    header('location:../../index.php');
}

?>