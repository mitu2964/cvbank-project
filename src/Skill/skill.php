<?php
namespace App\Skill;
use PDO;
class skill
{
    private $id = "";
    private $postid = "";
    private $mainid = "";
    private $title = "";
    private $description = "";
    private $level = "";
    private $experience = "";
    private $experience_area = "";
    private $pdo = "";

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');

    }

    public function setData($data = '')
    {
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }
        if (array_key_exists('level', $data)) {
            $this->level = $data['level'];
        }
        if (array_key_exists('experience', $data)) {
            $this->experience = $data['experience'];
        }
        if (array_key_exists('experience_area', $data)) {
            $this->experience_area = $data['experience_area'];
        }
        if (array_key_exists('postid', $data)) {
            $this->postid = $data['postid'];
        }
        if (array_key_exists('mainid', $data)) {
            $this->mainid = $data['mainid'];
        }
        return $this;
    }//End of setData

    public function store()
    {
        try {
            $query = "INSERT INTO `skills` (`id`, `user_id`, `title`,`description`,`level`,`experience`,`experience_area`)
                      VALUES (:id,:uid,:title,:description,:level,:experience,:experience_area)";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => null,
                    ':uid' => $this->id,
                    ':title' => $this->title,
                    ':description' => $this->description,
                    ':level' => $this->level,
                    ':experience' => $this->experience,
                    ':experience_area' => $this->experience_area,
                )
            );
            if ($stmnt) {
                $_SESSION['skill-message'] = "Successfully Data Stored";
                header("location:add_skill.php?id=$this->id");
            }
        } catch (PDOException $e) {
            echo 'Error' . $e->getMessage();
        }


    }//End of store
    public function show(){
        try{
            $query = "SELECT * FROM `skills` WHERE `user_id` = "."'".$this->id."'";
//            echo $query;
//            die();
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetchAll();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of show
    public function skillshow(){
        try{
            $query = "SELECT * FROM `skills` WHERE `id` = "."'".$this->id."'";
//            echo $query;
//            die();
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of show
    public function update()
    {
        try {
            $query = 'UPDATE skills SET `id`=:id,`user_id`=:uid,`title`=:title,`description`=:description,
                      `level`=:level, `experience`=:experience, `experience_area`=:experience_area WHERE id = :id';
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => $this->postid,
                    ':uid' => $this->id,
                    ':title' => $this->title,
                    ':description' => $this->description,
                    ':level' => $this->level,
                    ':experience' => $this->experience,
                    ':experience_area' => $this->experience_area,
                )
            );
            if ($stmnt) {
                $_SESSION['skill-message'] = "Successfully Data Updated";
                header("location:update_skill.php?id=$this->postid");
            }
        }
        catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of update
    public function delete()
    {
        try {
            $query = "DELETE FROM `skills` WHERE `skills`.`id` =". $this->id;
            $stmnt = $this->pdo->query($query);
            $stmnt->execute();
            if ($stmnt) {
                $_SESSION['message'] = "Successfully Data Deleted";
                header("location:../Skills/skill_view.php?id=$this->mainid");
            }
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of Delete
}