<?php

namespace App\Profile;
use PDO;
class profile
{
    private $id='';
    private $postid='';
    private $username='';
    private $user_id='';
    private $mainid='';
    private $title='';
    private $no_of_items='';
    private $searchvalue='';
    private $img='';
    private $keywords='';
    private $pdo='';

    public function __construct()
    {
        session_start();
        $this->pdo= new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
    }

    public function setData($data=''){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
		if(array_key_exists('keywords',$data)){
            $this->keywords=$data['keywords'];
        }
		if(array_key_exists('searchvalue',$data)){
            $this->searchvalue=$data['searchvalue'];
        }
        if(array_key_exists('postid',$data)){
            $this->postid=$data['postid'];
        }
        if(array_key_exists('username',$data)){
            $this->username=$data['username'];
        }
        if (array_key_exists('img', $data)) {
            $this->img= $data['img'];
        }
        if(array_key_exists('title',$data)){
            $this->title=$data['title'];
        }
        if(array_key_exists('no_of_items',$data)){
            $this->no_of_items=$data['no_of_items'];
        }
        if(array_key_exists('mainid',$data)){
            $this->mainid=$data['mainid'];
        }
        return $this;
    }//End of setData

    public function searchresult($perpage='',$offset=''){
        try{
            $key = "'%".$this->keywords."%'";
            $searchvalue = $this->searchvalue;
            if ($searchvalue == 'skills'){
                $query = "SELECT SQL_CALC_FOUND_ROWS users.id as uid, users.username as username, abouts.bio as ab_bio,abouts.title as ab_title, 
				settings.img as simg,settings.fullname as name, skills.title as sktitle FROM `users` INNER JOIN abouts ON users.id = abouts.user_id 
				JOIN skills ON users.id = skills.user_id JOIN settings ON users.id = settings.user_id WHERE skills.title LIKE $key LIMIT $perpage OFFSET $offset";
            }elseif($searchvalue == 'educations'){
                $query = "SELECT SQL_CALC_FOUND_ROWS users.id as uid, users.username as username, abouts.bio as ab_bio,abouts.title as ab_title, settings.img as simg,settings.fullname as name, 
				educations.title as edutitle FROM `users` INNER JOIN abouts ON users.id = abouts.user_id 
				JOIN educations ON users.id = educations.user_id JOIN settings ON users.id = settings.user_id WHERE educations.title LIKE $key 
				LIMIT $perpage OFFSET $offset";
            }
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetchAll();
            $subquery = 'SELECT FOUND_ROWS()';
            $totalrow = $this->pdo->query($subquery)->fetch(PDO::FETCH_COLUMN);

            $value['totalrow'] = $totalrow;
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of index
    public function usernameid(){
        try{
            $query = "SELECT id FROM `users` WHERE `username` = "."'".$this->username."'";
            //          echo $query;
//            die();
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $about_value = $stmnt->fetch();
            return $about_value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of about show
    public function aboutshow(){
        try{
            $query = "SELECT * FROM `abouts` WHERE `user_id` = "."'".$this->id."'";
//            echo $query;
//            die();
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $about_value = $stmnt->fetch();
            return $about_value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of about show
    public function hobbiesshow(){
        try{
            $query = "SELECT * FROM `hobbies` WHERE `user_id` = "."'".$this->id."'";
//            echo $query;
//            die();
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $hobbies_value = $stmnt->fetchAll();
            return $hobbies_value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of hobbies show
    public function factshow(){
        try{
            $query = "SELECT * FROM `facts` WHERE `user_id` = "."'".$this->id."'";
//            echo $query;
//            die();
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $fact_value = $stmnt->fetchAll();
            return $fact_value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of fact show
    public function educationshow(){
        try{
            $query = "SELECT * FROM `educations` WHERE `user_id` = "."'".$this->id."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $edu_value = $stmnt->fetchAll();
            return $edu_value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of fact show
    public function skillshow(){
        try{
            $query = "SELECT * FROM `skills` WHERE `user_id` = "."'".$this->id."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $skill_value = $stmnt->fetchAll();
            return $skill_value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of fact show
    public function awardshow(){
        try{
            $query = "SELECT * FROM `awards` WHERE `user_id` = "."'".$this->id."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $award_value = $stmnt->fetchAll();
            return $award_value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of fact show
    public function settingshow(){
        try{
            $query = "SELECT * FROM `settings` WHERE `user_id` = "."'".$this->id."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $setting_value = $stmnt->fetch();
            return $setting_value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of fact show
    public function usersshow(){
        try{
            $query = "SELECT * FROM `users` WHERE `id` = "."'".$this->id."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $user_value = $stmnt->fetch();
            return $user_value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of fact show
    public function portfolioshow(){
        try{
            $query = "SELECT * FROM `portfolios` WHERE `user_id` = "."'".$this->id."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $portfolio_value = $stmnt->fetchAll();
            return $portfolio_value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of fact show
    public function serviceshow(){
        try{
            $query = "SELECT * FROM `services` WHERE `user_id` = "."'".$this->id."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $service_value = $stmnt->fetchAll();
            return $service_value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of fact show
    public function blogshow(){
        try{
            $query = "SELECT * FROM `posts` WHERE `user_id` = "."'".$this->id."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $blog_value = $stmnt->fetchAll();
            return $blog_value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of fact show
}