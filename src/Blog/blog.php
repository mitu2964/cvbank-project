<?php

namespace App\Blog;
use PDO;
class blog
{
    private $id = "";
    private $postid = "";
    private $mainid = "";
    private $title = "";
    private $description = "";
    private $tags = "";
    private $categories = "";
    private $pdo = "";

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');

    }

    public function setData($data = '')
    {
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }
        if (array_key_exists('tags', $data)) {
            $this->tags = $data['tags'];
        }
        if (array_key_exists('categories', $data)) {
            $this->categories = $data['categories'];
        }
        if (array_key_exists('postid', $data)) {
            $this->postid = $data['postid'];
        }
        if (array_key_exists('mainid', $data)) {
            $this->mainid = $data['mainid'];
        }
        return $this;
    }//End of setData

    public function store()
    {
        try {
            $query = "INSERT INTO `posts` (`id`, `user_id`, `title`,`description`,`tags`,`categories`)
                      VALUES (:id,:uid,:title,:description,:tags,:categories)";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => null,
                    ':uid' => $this->id,
                    ':title' => $this->title,
                    ':description' => $this->description,
                    ':tags' => $this->tags,
                    ':categories' => $this->categories,
                )
            );
            if ($stmnt) {
                $_SESSION['blog-message'] = "Successfully Data Stored";
                header("location:add_blog.php?id=$this->id");
            }
        } catch (PDOException $e) {
            echo 'Error' . $e->getMessage();
        }


    }//End of store
    public function show(){
        try{
            $query = "SELECT * FROM `posts` WHERE `user_id` = "."'".$this->id."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetchAll();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of show
     public function updateshow(){
        try{
            $query = "SELECT * FROM `posts` WHERE `id` = "."'".$this->id."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of show
    public function update()
    {
        try {
            $query = 'UPDATE posts SET `id`=:id,`user_id`=:uid,`title`=:title,`description`=:description,
                      `tags`=:tags, `categories`=:categories WHERE id = :id';
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => $this->postid,
                    ':uid' => $this->id,
                    ':title' => $this->title,
                    ':description' => $this->description,
                    ':tags' => $this->tags,
                    ':categories' => $this->categories,
                )
            );
            if ($stmnt) {
                $_SESSION['update-message'] = "Successfully Data Updated";
                header("location:update_blog.php?id=$this->postid");
            }
        }
        catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of update
    public function delete()
    {
        try {
            $query = "DELETE FROM `posts` WHERE `posts`.`id` =". $this->id;
            $stmnt = $this->pdo->query($query);
            $stmnt->execute();
            if ($stmnt) {
                $_SESSION['message'] = "Successfully Data Deleted";
                header("location:../Blogs/blog_view.php?id=$this->mainid");
            }
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of Delete
}