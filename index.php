
<!doctype html>

<html>

   <head>
   <meta charset="UTF-8">

      <title>Search Your CV Here</title>
      <link rel="stylesheet" type="text/css" href="assets/css/styles.css">
       <style>
           input{
               border: 2px solid #1A237E;
           }
           p{
               color: #000;
               display: inline-block;
               margin-right: 5px;
           }
           button{
               background-color: #1A237E;
               color: #ffffff;
               border: 1px solid #000;
           }
       </style>
   </head>

   <body>

      <div id="header_buttons">

         <ul>

            <li id="sign_in"><a id="sign-in-text" href="login_form.php">Sign In</a></li>

         </ul>

      </div>

      <div id="google_logo">

         <img src="assets/images/reflection.png">

      </div>
      <form action="search_result.php" method="get">
          <div id=query>

             <input type="text" name="keywords"required autofocus/>

          </div>

          <div class="buttons">
                Search By:
                <input type="radio" name="searchvalue" value="skills"checked> <p>Skills</p>
                <input type="radio" name="searchvalue" value="educations"><p>Education Qualification</p>
                <button  type="submit" value="Submit" id="search">Search</button><br><br><br>
                <div>
                    <?php
                        session_start();
                        if (isset($_SESSION['sr'])){
                        echo $_SESSION['sr'];
                        unset($_SESSION['sr']);
                        }
                    ?>
                </div>
          </div>
	  </form>
      <div id="footer">


      </div>

   </body>

</html>
